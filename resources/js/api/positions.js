import axios from "axios";

const client=axios.create({
    baseURL:"/api"
});

export default {
    all(){
        return client.get("positions");
    },
    find(id){
        return client.get("positions/"+id);
    },
    create(data){
        return client.post("positions",data);
    },
    update(id,data){
        return client.put("positions/"+id,data);
    },
    delete(id){
        return client.delete("positions/"+id);
    }
}
