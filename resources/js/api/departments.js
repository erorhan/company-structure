import axios from "axios";

const client=axios.create({
   baseURL:'/api',
});


export  default {
    all(){
       return  client.get('departments');
    },
    create(data){
      return client.post("departments",data);
    },
    find(id){
        return client.get('departments/'+id);
    },
    update(id,data){
        return client.put('departments/'+id,data);
    },
    delete(id){
        return client.delete("departments/"+id);
    }

}
