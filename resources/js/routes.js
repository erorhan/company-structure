import Home from "./components/Home";
import NotFound from "./components/NotFound";
//Department
import Dep_AddComponent from "./components/department/Add";
import Dep_EditComponent from "./components/department/Edit";
import Dep_ListComponent from "./components/department/List";
//Employee
import Emp_AddComponent from './components/employee/Add';
import Emp_ListComponent from './components/employee/List';
import Emp_EditComponent from './components/employee/Edit';
//Position
import Pos_AddComponent from './components/position/Add';
import Pos_ListComponent from './components/position/List';
import Pos_EditComponent from './components/position/Edit';
export const routes=[
    {
        path:'/',
        name:'home',
        component:Home
    },
    {
        path:'/404',
        name:'404',
        component:NotFound,
    },
    {
        path:'*',
        redirect:'/404'
    },
    //department
    {
        path:'/dep-add',
        name:'department-add',
        component: Dep_AddComponent,
    },
    {
        path:'/dep-edit/:id/edit',
        name:'department-edit',
        component:Dep_EditComponent,
    },
    {
        path:'/dep-list',
        name:'department-list',
        component:Dep_ListComponent,
    },
    //employee
    {
        path:'/emp-add',
        name:'employee-add',
        component:Emp_AddComponent,
    },
    {
        path:'/emp-edit',
        name:'employee-edit',
        component:Emp_EditComponent,
    },
    {
        path:'/emp-list',
        name:'employee-list',
        component:Emp_ListComponent,
    },
    //position
    {
        path:'/pos-add',
        name:'position-add',
        component:Pos_AddComponent,
    },
    {
        path:'/pos-edit',
        name:'position-edit',
        component:Pos_EditComponent,
    },
    {
        path:'/pos-list',
        name:'position-list',
        component:Pos_ListComponent,
    },
];
