

require('./bootstrap');

window.Vue = require('vue').default;
import VueRouter from "vue-router";
Vue.use(VueRouter);

import App from './components/App';

import {routes} from './routes';
const router=new VueRouter({
    mode:'history',
    routes,
});


const app = new Vue({
    el: '#app',
    components:{App},
    router,
});
