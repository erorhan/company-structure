<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee', function (Blueprint $table) {
            $table->increments("id");
            $table->integer("department_id")->unsigned();
            $table->integer("position_id")->unsigned();
            $table->integer("chief_id")->unsigned()->nullable();
            $table->decimal("salary",6,3);
            $table->date("startDate");
            $table->date("endDate")->nullable();
            $table->string("firstName");
            $table->string("lastName");
            $table->date("dateOfBirth")->nullable();
            $table->string("identificationNumber")->unsigned();
            $table->boolean("gender");
            $table->string("phone");
            $table->string("email")->nullable();
            $table->string("address");
            $table->string("imageUrl")->nullable();
            $table->string("profession");
            $table->timestamps();
            $table->softDeletes();

            $table->foreign("department_id")->references("id")->on("department")->onDelete("cascade");
            $table->foreign("position_id")->references("id")->on("position")->onDelete("cascade");
            $table->foreign("chief_id")->references("id")->on("employee");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee');
    }
}
