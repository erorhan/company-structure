<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\DepartmentController;
use App\Http\Controllers\Api\PositionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::namespace('Api')->group(function (){
    //Departments
    Route::get("/departments",[DepartmentController::class,"getAll"]);
    Route::get('/departments/{department}',[DepartmentController::class,"show"]);
    Route::put('/departments/{department}',[DepartmentController::class,"update"]);
    Route::delete('/departments/{department}',[DepartmentController::class,"destroy"]);
    Route::post('/departments',[DepartmentController::class,"store"]);
    Route::get('/departments/positions/{department}',[DepartmentController::class,"getPositionsInDepartment"]);

    //Positions
    Route::get("/positions",[PositionController::class,"getAll"]);
    Route::get('/positions/{position}',[PositionController::class,"show"]);
    Route::put('/positions/{position}',[PositionController::class,"update"]);
    Route::delete('/positions/{position}',[PositionController::class,"destroy"]);
    Route::post('/positions',[PositionController::class,"store"]);


});


