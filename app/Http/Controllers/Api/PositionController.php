<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PositionResource;
use App\Models\Position;
use Illuminate\Http\Request;

class PositionController extends Controller
{
    public function getAll(){
        return PositionResource::collection(Position::all());
    }

    public function show(Position $position){
        return new PositionResource($position);
    }

    public function update(Position $position,Request $request){
        $data=$request->validate([
           "name"=>"required",
           "description"=>"max:80"
        ]);

        $position->update($data);

        return new PositionResource($position);
    }

    public function destroy(Position $position){
        $position->delete();

        return response(null,"204");
    }

    public function store(Request $request){
        $data=$request->validate([
           "name"=>"required",
           "description"=>"max:80"
        ]);

        return new PositionResource(Position::create([
            "name"=>$data["name"],
            "description"=>$data["description"]
        ]));
    }
}
