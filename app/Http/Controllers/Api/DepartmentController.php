<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\DepartmentResource;
use App\Models\Department;
use Illuminate\Http\Request;
use PhpParser\Node\Param;
use function MongoDB\BSON\toJSON;

class DepartmentController extends Controller
{
    public function getAll(){
        return DepartmentResource::collection(Department::all());
    }

    public function show(Department $department){
        return new DepartmentResource($department);
    }

    public function update(Department $department,Request $request){
        $data=$request->validate([
            "name"=>"required",
            "description"=>"max:80"
        ]);

        $department->update($data);

        return new DepartmentResource($department);
    }

    public function destroy(Department $department){
        $department->delete();

        return response(null,204);
    }

    public function store(Request $request){
        $data=$request->validate([
           "name"=>"required",
           "description"=>"max:80"
        ]);


        return new DepartmentResource(Department::firstOrCreate(
            ["name"=>$data["name"]],
            ["name"=>$data["name"],"description"=>$data["description"]])
        );
    }

}
