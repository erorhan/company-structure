<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table="department";
    protected $guarded=["id"];

    public function positions(){
        return $this->belongsToMany(Position::class,"position_in_department");
    }

}
